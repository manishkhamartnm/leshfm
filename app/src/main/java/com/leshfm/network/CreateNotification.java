package com.leshfm.network;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;


import androidx.core.app.NotificationCompat;

import com.leshfm.R;

public class CreateNotification {

    public  static String ACTION_PLAY = "action_play";
    public  static String ACTION_PAUSE = "action_pause";

    public static NotificationCompat.Builder builder;
    public static Notification notification;

    public static void createNotification(Context context, int playbutton) {
        //MediaSessionCompat mediaSessionCompat = new MediaSessionCompat(context, "tag");
        Intent play = new Intent(context, NotificationActionService.class).setAction(ACTION_PLAY);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, play, PendingIntent.FLAG_UPDATE_CURRENT);
        //Notification.MediaStyle style = new Notification.MediaStyle();

       // androidx.core.app.NotificationCompat.MediaStyle style = new androidx.core.app.NotificationCompat.MediaStyle();

       /*NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context, context.getResources().getString(R.string.channelId));*/

        Notification.Builder builder = new Notification.Builder(context);
        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Media Title")
                .setContentText("Media Artist");
                //.setDeleteIntent(pendingIntent)
                //.setStyle(new NotificationCompat.MediaStyle());

        builder.addAction(generateAction(Icon.createWithResource(context, android.R.drawable.ic_media_pause), "Play", context,pendingIntent));
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null){
            notification = builder.build();
           // notificationManager.notify(0,notification);
        }

    }

    private static Notification.Action generateAction(Icon icon, String title, Context context, PendingIntent pendingIntent) {
        return new Notification.Action.Builder(icon, title, pendingIntent).build();
    }

}
