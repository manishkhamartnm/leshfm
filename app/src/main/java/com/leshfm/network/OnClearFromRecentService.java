package com.leshfm.network;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.widget.RemoteViews;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.leshfm.R;

public class OnClearFromRecentService extends Service {
    NotificationCompat.Builder builder;
    Intent play;
    PendingIntent pendingIntent;
    RemoteViews contentView;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent!= null){
            String title = "", artist = "";
            int id = intent.getIntExtra("player", 0);
            if (intent.getStringExtra("title") != null)
                title = intent.getStringExtra("title");

            if (intent.getStringExtra("artist") != null)
                artist = intent.getStringExtra("artist");

            startForegroundNotification(id, title, artist);
        }


        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopSelf();

        stopForeground(true);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        stopForeground(true);
        stopSelf();
    }

    public void startForegroundNotification(int action, String title, String artist) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel();
        }
        builder = new NotificationCompat.Builder(this, getResources().getString(R.string.channelId));
        play = new Intent(this, NotificationActionService.class).setAction(CreateNotification.ACTION_PLAY);
        pendingIntent = PendingIntent.getBroadcast(this, 0, play, PendingIntent.FLAG_UPDATE_CURRENT);
        contentView = new RemoteViews(getPackageName(), R.layout.custom_bg_notification);
        contentView.setTextViewText(R.id.title, title);
       // contentView.setTextViewText(R.id.artist, artist);
        if (action == 1) {
            contentView.setImageViewResource(R.id.imgaction, R.mipmap.play);
        } else {
            contentView.setImageViewResource(R.id.imgaction, R.mipmap.pause);
        }
        contentView.setOnClickPendingIntent(R.id.imgaction, pendingIntent);

        builder.setCustomContentView(contentView);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        CreateNotification.notification = builder.setOngoing(true).build();
        startForeground(101, CreateNotification.notification);
    }

    private void createChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(getResources().getString(R.string.channelId),
                    getResources().getString(R.string.channelId), NotificationManager.IMPORTANCE_LOW);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notificationChannel.setShowBadge(false);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null)
                notificationManager.createNotificationChannel(notificationChannel);
        }
    }
}
