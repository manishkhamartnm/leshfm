package com.leshfm.network;

public interface Playable {
    void onTrackPlay();
    void onTrackPause();
}
