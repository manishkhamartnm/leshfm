package com.leshfm;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.leshfm.R;
import com.leshfm.fragment.FollowUsFragment;
import com.leshfm.fragment.HomeFragment;
import com.leshfm.fragment.SponsorsFragment;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private AppBarConfiguration mAppBarConfiguration;
    DrawerLayout drawer;
    ImageView playing_icon, sponsors_icon, follow_icon, close_btn;
    TextView playing_txt, sponsors_txt, follow_txt;
    Typeface regular, medium, bold, light;
    LinearLayout playing_layout, sponsors_layout, follow_layout, nav_watch_the_cube, nav_contact_us, nav_dj_roster, nav_featured, nav_about_us;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        playing_icon = findViewById(R.id.playing_icon);
        sponsors_icon = findViewById(R.id.sponsors_icon);
        follow_icon = findViewById(R.id.follow_icon);
        playing_txt = findViewById(R.id.playing_txt);
        sponsors_txt = findViewById(R.id.sponsors_txt);
        follow_txt = findViewById(R.id.follow_txt);
        playing_layout = findViewById(R.id.playing_layout);
        playing_layout.setOnClickListener(this);
        sponsors_layout = findViewById(R.id.sponsors_layout);
        sponsors_layout.setOnClickListener(this);
        follow_layout = findViewById(R.id.follow_layout);
        follow_layout.setOnClickListener(this);

        navigationView = findViewById(R.id.nav_view);
        int width = getResources().getDisplayMetrics().widthPixels;
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) navigationView.getLayoutParams();
        params.width = width;
        navigationView.setLayoutParams(params);

        View headerView = navigationView.getHeaderView(0);
        close_btn = headerView.findViewById(R.id.close_btn);
        close_btn.setOnClickListener(this);
        nav_about_us = headerView.findViewById(R.id.nav_about_us);
        nav_about_us.setOnClickListener(this);

        nav_dj_roster = headerView.findViewById(R.id.nav_dj_roster);
        nav_dj_roster.setOnClickListener(this);


        nav_featured = headerView.findViewById(R.id.nav_featured);
        nav_featured.setOnClickListener(this);
        nav_contact_us = headerView.findViewById(R.id.nav_contact_us);
        nav_contact_us.setOnClickListener(this);
        nav_watch_the_cube = headerView.findViewById(R.id.nav_watch_the_cube);
        nav_watch_the_cube.setOnClickListener(this);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.playing_layout, R.id.nav_about_us, R.id.nav_featured, R.id.sponsors_layout, R.id.nav_watch_the_cube, R.id.nav_contact_us, R.id.follow_layout)
                .setDrawerLayout(drawer)
                .build();
        /*NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);*/

        light = ResourcesCompat.getFont(this, R.font.latolight);
        regular = ResourcesCompat.getFont(this, R.font.latoregular);
        medium = ResourcesCompat.getFont(this, R.font.latomedium);
        bold = ResourcesCompat.getFont(this, R.font.latobold);
        enableFooter(playing_layout);
    }

    public void toggleDrawer() {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END, true);
        } else {
            drawer.openDrawer(GravityCompat.END, true);
        }
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void enableFooter(View v) {
        resetFooter();
        switch (v.getId()) {
            case R.id.playing_layout:
                playing_icon.setColorFilter(ContextCompat.getColor(this, R.color.violet_color), PorterDuff.Mode.SRC_ATOP);
                playing_txt.setTextColor(getResources().getColor(R.color.violet_color, null));
                playing_txt.setTypeface(bold);
                loadFragment(new HomeFragment());
                break;
            case R.id.sponsors_layout:
                sponsors_icon.setColorFilter(ContextCompat.getColor(this, R.color.violet_color), PorterDuff.Mode.SRC_ATOP);
                sponsors_txt.setTypeface(bold);
                sponsors_txt.setTextColor(getResources().getColor(R.color.violet_color, null));
                loadFragment(new SponsorsFragment());
                break;
            case R.id.follow_layout:
                follow_icon.setColorFilter(ContextCompat.getColor(this, R.color.violet_color), PorterDuff.Mode.SRC_ATOP);
                follow_txt.setTypeface(bold);
                follow_txt.setTextColor(getResources().getColor(R.color.violet_color, null));
                loadFragment(new FollowUsFragment());
                break;
        }
    }

    public void resetFooter() {
        playing_icon.setColorFilter(ContextCompat.getColor(this, R.color.white_color), PorterDuff.Mode.SRC_ATOP);
        playing_txt.setTextColor(getResources().getColor(R.color.white_color, null));
        playing_txt.setTypeface(light);
        sponsors_icon.setColorFilter(ContextCompat.getColor(this, R.color.white_color), PorterDuff.Mode.SRC_ATOP);
        sponsors_txt.setTextColor(getResources().getColor(R.color.white_color, null));
        sponsors_txt.setTypeface(light);
        follow_icon.setColorFilter(ContextCompat.getColor(this, R.color.white_color), PorterDuff.Mode.SRC_ATOP);
        follow_txt.setTextColor(getResources().getColor(R.color.white_color, null));
        follow_txt.setTypeface(light);
    }

    public void onBackButton() {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            Fragment fragment = getVisibleFragment();

            if (fragment != null && !fragment.getClass().toString().contains("HomeFragment") && !fragment.getClass().toString().contains("NavHostFragment")) {
                Log.i("fragment", fragment.getClass().toString());
                super.onBackPressed();
            } else {
                methodExitDialog();
            }
        }

    }

    public Fragment getVisibleFragment() {
        FragmentManager childfragmentManager = null;
        FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        for (Fragment fragment : fragments) {
            if (fragment != null && fragment.isVisible())
                childfragmentManager = fragment.getChildFragmentManager();
            if (childfragmentManager != null) {
                List<Fragment> childfragments = childfragmentManager.getFragments();
                for (Fragment childfragment : childfragments) {
                    if (childfragment != null && childfragment.isVisible())
                        return childfragment;
                }
            }
            return fragment;
        }

        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        onBackButton();
    }

    void methodExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.playing_layout:
                enableFooter(playing_layout);
                break;
            case R.id.sponsors_layout:
                enableFooter(sponsors_layout);
                break;
            case R.id.follow_layout:
                enableFooter(follow_layout);
                break;
            case R.id.close_btn:
                toggleDrawer();
                break;
            case R.id.nav_about_us:
                startActivityForResult(new Intent(this, AboutUsScreen.class), 101);
                break;
            case R.id.nav_featured:
                startActivityForResult(new Intent(this, FeaturedScreen.class), 101);
                break;
            case R.id.nav_watch_the_cube:
                startActivityForResult(new Intent(this, WatchTheCubeScreen.class), 101);
                break;
            case R.id.nav_contact_us:
                startActivityForResult(new Intent(this, ContactUsScreen.class), 101);
                break;
            case R.id.nav_dj_roster:
                startActivityForResult(new Intent(this, DjrosterScreen.class), 101);
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            toggleDrawer();
        }
    }
}
