package com.leshfm;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.ui.AppBarConfiguration;

import com.leshfm.R;

public class LandingScreen extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    ImageView play_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_landing_screen);
        play_btn =  findViewById(R.id.play_btn);
        play_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LandingScreen.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

}
