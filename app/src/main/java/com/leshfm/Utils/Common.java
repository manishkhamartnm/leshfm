package com.leshfm.Utils;

public class Common {

    public static String about_us_url = "https://www.leshfm.com/about";
    public static String djroster_link = "https://leshfm.com/dj-roster";
    public static String featured_link = "https://www.leshfm.com/featured-djs";
    public static String sponsors_link = "https://www.leshfm.com/sponsers";
    public static String contact_us_link = "https://www.leshfm.com/contact";
	 public static String watch_the_cube_link = "https://leshfm.com/watch-the-cube-live";
    public static String follow_us_link = "https://www.leshfm.com/social";
    public static String stream_url = "http://uk1.internet-radio.com:8315/";
    public static String ACTION_BRODACAST = "com.leshfm.playpauselistener";

    public static String title;
    public static String artist;

    public static boolean isplaying = false;



}
