package com.leshfm.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Preference {

    SharedPreferences preferences;

    public Preference(Context c, String prefname) {
        preferences = c.getSharedPreferences(prefname, Context.MODE_PRIVATE);
    }

    public Editor edit() {
        return preferences.edit();
    }

    /**
     * Get Preference String
     *
     * @param key Pref Key
     * @return String
     */
    public String getString(String key) {
        try {
            return preferences.getString(key, "");
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * Get Preference Int
     *
     * @param key Pref Key
     * @param def Def
     * @return Int
     */
    public int getInt(String key, int def) {
        return preferences.getInt(key, 0);
    }

    /**
     * Get Preference Long
     *
     * @param key Pref Key
     * @param def Def
     * @return Long
     */
    public long getLong(String key, long def) {
        return preferences.getLong(key, 0);
    }


    /**
     * Get Preference Boolean
     *
     * @param key Pref Key
     * @param def Def
     * @return Boolean
     */
    public boolean getBoolean(String key, Boolean def) {
        return preferences.getBoolean(key, def);
    }


    /**
     * Check Key
     *
     * @param key Key
     * @return Boolean
     */
    public boolean haskey(String key) {
        return preferences.contains(key);
    }
}
