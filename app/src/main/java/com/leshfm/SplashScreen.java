package com.leshfm;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.leshfm.R;


public class SplashScreen extends Activity {

    ImageView logo;
    Animation animZoomin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_splshscreen);

        logo = findViewById(R.id.logo);

        animZoomin = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
        logo.startAnimation(animZoomin);
        redirectScreen(9000);
       try {
            MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.leshfm);
            mediaPlayer.start();
        } catch (Exception e) {
            redirectScreen(9000);
        }
    }


    void redirectScreen(int timer){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }, timer);
    }
}
