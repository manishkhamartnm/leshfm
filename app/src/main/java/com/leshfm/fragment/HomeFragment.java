package com.leshfm.fragment;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaMetadata;
import android.media.MediaPlayer;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.leshfm.MainActivity;
import com.leshfm.R;
import com.leshfm.Utils.Common;
import com.leshfm.network.CreateNotification;
import com.leshfm.network.OnClearFromRecentService;
import com.leshfm.network.Playable;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import static com.leshfm.Utils.Common.ACTION_BRODACAST;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements SeekBar.OnSeekBarChangeListener, Playable {

    private MediaPlayer player;
    private ImageView playeraction;//mutevolume
    private AudioManager audioManager;
    private RelativeLayout init_layout;
    private LinearLayout player_layout;
    private TextView state;
    private TextView title;
    private static int lastvolume, playerId;
    private boolean isBluetoothDisconnected = false;

    private Timer timer;
    private MediaSession mMediaSession;

    private Activity activity;

    private int currentSeekPosition = 0;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString("PARAM1", param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();

        if (mMediaSession == null) {
            Log.d("init()", "Using MediaSession API.");

            mMediaSession = new MediaSession(activity, "PlayerServiceMediaSession");
            mMediaSession.setFlags(MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS);
            mMediaSession.setActive(true);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        audioManager = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);

        // Use the music strea
        // initializeMediaPlayer();
        ImageView menu_btn = view.findViewById(R.id.menu_btn);
        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ((MainActivity) activity).toggleDrawer();
                } catch (Exception e) {
                }
            }
        });

        Typeface regular = ResourcesCompat.getFont(activity, R.font.latoregular);
        Typeface medium = ResourcesCompat.getFont(activity, R.font.latomedium);
        Typeface bold = ResourcesCompat.getFont(activity, R.font.latobold);

        state = view.findViewById(R.id.state);
        state.setTypeface(bold);
        title = view.findViewById(R.id.title);
        title.setTypeface(bold);
        TextView artist = view.findViewById(R.id.artist);
        artist.setTypeface(regular);
        playeraction = view.findViewById(R.id.playeraction);
        SeekBar volumeBar = view.findViewById(R.id.volumeBar);
        volumeBar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        lastvolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        volumeBar.setProgress(lastvolume);
        volumeBar.setOnSeekBarChangeListener(this);
        player_layout = view.findViewById(R.id.player_layout);
        init_layout = view.findViewById(R.id.init_layout);
        if (player != null && player.isPlaying()) {
            init_layout.setVisibility(View.GONE);
            player_layout.setVisibility(View.VISIBLE);
        }
        RelativeLayout playbtn = view.findViewById(R.id.playbtn);
        playbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentSeekPosition = 0;
                boolean flag = requestAudioFocusForMyApp();
                if (!flag) {
                    Toast.makeText(activity, activity.getResources().getString(R.string.phonestring), Toast.LENGTH_LONG).show();
                    return;
                }
                init_layout.setVisibility(View.GONE);
                player_layout.setVisibility(View.VISIBLE);
                if (player != null) {
                    if (!player.isPlaying()) {
                        startPlaying();
                    }
                } else
                    initializeMediaPlayer(0);
            }
        });
        RelativeLayout playpausebtn = view.findViewById(R.id.playpausebtn);
        playpausebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentSeekPosition = 0;
                if (player != null) {
                    if (player.isPlaying()) {
                        stopPlaying();
                    } else {
                        startPlaying();
                    }
                } else
                    initializeMediaPlayer(0);
            }
        });
        requestAudioFocus();
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        //IntentFilter filter1 = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
       /* filter1.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter1.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);*/
        //filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        activity.registerReceiver(mReceiver, filter);
        //initializeMediaPlayer();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //  releaseAudioFocusForMyApp(activity);
    }

    private void initializeMediaPlayer(final int currentPosition) {
        if(player != null) {
            player.release();
        }
        player = new MediaPlayer();
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                player.setAudioAttributes(
                        new AudioAttributes
                                .Builder()
                                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                                .build());
            } else {
                player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            }
            player.setDataSource(Common.stream_url);
            player.setWakeMode(activity, PowerManager.PARTIAL_WAKE_LOCK);
            player.prepareAsync();
            player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                public void onPrepared(MediaPlayer mp) {
                    if(currentPosition > 0) {
                        mp.seekTo(currentPosition);
                    }
                    player.start();
                    state.setText(getResources().getString(R.string.play_now));
                    playeraction.setImageResource(R.mipmap.pause);
                    service();
                }
            });

            player.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    if (what == MediaPlayer.MEDIA_ERROR_SERVER_DIED)
                        mp.reset();

                    else if (what == MediaPlayer.MEDIA_ERROR_UNKNOWN)
                        mp.reset();

                    initializeMediaPlayer(0);
                    return true;
                }
            });

            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {

                }
            });

        } catch (IllegalArgumentException | IllegalStateException | IOException e) {
            e.printStackTrace();
        }

    }

    private AudioManager.OnAudioFocusChangeListener focusChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                public void onAudioFocusChange(int focusChange) {
                    AudioManager am = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
                    switch (focusChange) {

                        case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK):
                            // Lower the volume while ducking.
                            player.setVolume(0.2f, 0.2f);
                            break;
                        case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT):
                            currentSeekPosition = player.getCurrentPosition();
                            player.pause();
                            break;

                        case (AudioManager.AUDIOFOCUS_LOSS):
                            stopPlaying();
                            /*ComponentName component = new ComponentName(activity, MediaControlReceiver.class);
                            am.unregisterMediaButtonEventReceiver(component);*/
                            break;

                        case (AudioManager.AUDIOFOCUS_GAIN):
                            // Return the volume to normal and resume if paused.
                            //  player.setVolume(1f, 1f);
                            // player.start();
                            if(!isBluetoothDisconnected) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                                    initializeMediaPlayer(currentSeekPosition);
                                } else {
                                    if (player == null) {
                                        initializeMediaPlayer(0);
                                    } else if (!player.isPlaying()) {
                                        startPlaying();
                                    }
                                }

                            }
                            break;
                        default:
                            break;
                    }
                }
            };

    private boolean requestAudioFocusForMyApp() {
        AudioManager am = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
        if (am == null) {
            return false;
        }
        // Request audio focus for playback
        int result = am.requestAudioFocus(focusChangeListener,
                // Use the music stream.
                AudioManager.STREAM_MUSIC,
                // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            Log.d("AudioFocus", "Audio focus received");
            return true;
        } else {
            Log.d("AudioFocus", "Audio focus NOT received");
            return false;
        }
    }

    private void releaseAudioFocusForMyApp(final Context context) {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (am != null) {
            am.abandonAudioFocus(null);
        }
    }

    private void service() {
        Intent serviceIntent = new Intent(getActivity(), OnClearFromRecentService.class);
        playerId = currentSeekPosition > 0 ? 2 : 0;
        serviceIntent.putExtra("player",  playerId);
        serviceIntent.putExtra("title", Common.title);
        serviceIntent.putExtra("artist", currentSeekPosition > 0 ? Common.artist : "");//Common.artist);
        activity.registerReceiver(actionbroadcaster, new IntentFilter(ACTION_BRODACAST));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ContextCompat.startForegroundService(activity, serviceIntent);
        } else {
            activity.startService(serviceIntent);
        }
        currentSeekPosition = 0;
        getMetaData();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void getMetaData() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                new MetadataTask().execute();
            }
        }, 0, 1000 * 60);
    }

    private void startPlaying() {
        isBluetoothDisconnected = false;
        boolean flag = requestAudioFocusForMyApp();
        if (!flag) {
            Toast.makeText(activity, activity.getResources().getString(R.string.phonestring), Toast.LENGTH_LONG).show();
            return;
        }
        // requestAudioFocusForMyApp(activity);
        if (player != null && !player.isPlaying()) {

            player.start();
            state.setText(getResources().getString(R.string.play_now));
            playeraction.setImageResource(R.mipmap.pause);

            Intent serviceIntent = new Intent(activity, OnClearFromRecentService.class);
            playerId = 2;
            serviceIntent.putExtra("player", playerId);
            serviceIntent.putExtra("title", Common.title);
            serviceIntent.putExtra("artist", Common.artist);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ContextCompat.startForegroundService(activity, serviceIntent);
            } else {
                activity.startService(serviceIntent);
            }
        }
    }

    private void stopPlaying() {
        isBluetoothDisconnected = false;
        try {
            if (player != null && player.isPlaying()) {
                player.pause();
                state.setText(getResources().getString(R.string.pause_now));
                playeraction.setImageResource(R.mipmap.play);
                Intent serviceIntent = new Intent(activity, OnClearFromRecentService.class);
                playerId = 1;
                serviceIntent.putExtra("player", playerId);
                serviceIntent.putExtra("title", Common.title);
                serviceIntent.putExtra("artist", Common.artist);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    ContextCompat.startForegroundService(activity, serviceIntent);
                } else {
                    activity.startService(serviceIntent);
                }
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Objects.requireNonNull(((AppCompatActivity) activity).getSupportActionBar()).hide();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int volume, boolean fromUser) {
        // mutevolume.setTag("full");
        // mutevolume.setImageResource(R.mipmap.ic_volume_up);
        lastvolume = volume;
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onTrackPlay() {
        requestAudioFocusForMyApp();
        player.start();
        playeraction.setImageResource(R.mipmap.pause);

        Intent serviceIntent = new Intent(getActivity(), OnClearFromRecentService.class);
        playerId = 2;
        serviceIntent.putExtra("player", playerId);
        serviceIntent.putExtra("title", Common.title);
        serviceIntent.putExtra("artist", Common.artist);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ContextCompat.startForegroundService(activity, serviceIntent);
        } else {
            activity.startService(serviceIntent);
        }
    }

    @Override
    public void onTrackPause() {
        requestAudioFocusForMyApp();
        player.pause();
        playeraction.setImageResource(R.mipmap.play);

        Intent serviceIntent = new Intent(getActivity(), OnClearFromRecentService.class);
        playerId = 1;
        serviceIntent.putExtra("player", playerId);
        serviceIntent.putExtra("title", Common.title);
        serviceIntent.putExtra("artist", Common.artist);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ContextCompat.startForegroundService(activity, serviceIntent);
        } else {
            activity.startService(serviceIntent);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            isBluetoothDisconnected = false;
            currentSeekPosition = 0;

            if (timer != null) {
                timer.cancel();
            }
            if (actionbroadcaster != null)
                activity.unregisterReceiver(actionbroadcaster);
            if (player != null && player.isPlaying())
                player.stop();
            if (player != null)
                player.release();
            Intent serviceIntent = new Intent(activity, OnClearFromRecentService.class);
            activity.stopService(serviceIntent);

            releaseAudioFocusForMyApp(activity);
        } catch (Exception e) {

        }

    }

    class MetadataTask extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {
            final StringBuilder builder = new StringBuilder();
            String titletxt = "LESHFM";
            try {
                Document doc = Jsoup.connect(Common.stream_url).get();
                // String title = doc.title();
                Elements links = doc.select("a[href]");

                //builder.append(title).append("\n");

                for (Element link : links) {
                    if (link.attr("href").contains("currentsong?sid=1")) {
                       /* builder.append("\n").append("Link : ").append(link.attr("href"))
                                .append("\n").append("Text : ").append(link.text());*/
                        titletxt = link.text();
                        break;
                    }
                }

            } catch (IOException e) {
                titletxt = "LESHFM";
            }
            return titletxt;
        }

        @Override
        protected void onPostExecute(String str) {
            super.onPostExecute(str);
            Common.title = str;
            title.setText(Common.title);
            Intent serviceIntent = new Intent(activity, OnClearFromRecentService.class);
            serviceIntent.putExtra("player", playerId);
            serviceIntent.putExtra("title", Common.title);
            serviceIntent.putExtra("artist", "");// Common.artist);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ContextCompat.startForegroundService(activity, serviceIntent);
            } else {
                activity.startService(serviceIntent);
            }
            try {
                AudioManager audioManager1 = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
                if (audioManager1 != null && audioManager1.isBluetoothA2dpOn()) {
                    onTrackChanged(Common.title);
                }
            } catch (Exception e) {

            }

        }
    }

    public class NotificationOnClickReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, Intent intent) {
            //Toast.makeText(activity, "Play", Toast.LENGTH_LONG).show();
        }
    }

    private BroadcastReceiver actionbroadcaster = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("in", "actionbroadcaster");
            if (intent != null && intent.getExtras() != null) {
                Log.i("in", "actionbroadcaster-2");
                String action = intent.getExtras().getString("actionname");
                Log.i("in", "actionbroadcaster-" + action);
                // if (intent.getAction() != null && intent.getAction().equals(CreateNotification.ACTION_PLAY)) {
                if (action != null && action.equalsIgnoreCase(CreateNotification.ACTION_PLAY)) {
                    Log.i("in", "actionbroadcaster-3");
                    if (player.isPlaying()) {
                        onTrackPause();
                    } else {
                        onTrackPlay();
                    }
                }
            }
        }
    };

    /*private boolean removeAudioFocus() {
        return AudioManager.AUDIOFOCUS_REQUEST_GRANTED ==
                audioManager.abandonAudioFocus(focusChangeListener);
    }*/

    private boolean requestAudioFocus() {
        AudioManager audioManager1 = (AudioManager) activity.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager1 != null) {
            int result = audioManager.requestAudioFocus(focusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
            if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                //Focus gained
                return true;
            }
        }
        //Could not gain focus
        return false;
    }

    private void onTrackChanged(String album) {
        try {
            MediaMetadata metadata = new MediaMetadata.Builder()
                    .putString(MediaMetadata.METADATA_KEY_TITLE, album)
                    .putString(MediaMetadata.METADATA_KEY_ARTIST, "LESHFM")
                    /* .putString(MediaMetadata.METADATA_KEY_ALBUM, album)
                     .putLong(MediaMetadata.METADATA_KEY_DURATION, 1050)
                     .putLong(MediaMetadata.METADATA_KEY_TRACK_NUMBER, 1)*/
                    .build();

            mMediaSession.setMetadata(metadata);

            PlaybackState state = new PlaybackState.Builder()
                    .setActions(PlaybackState.ACTION_PLAY)
                    .setState(PlaybackState.STATE_PLAYING, 1, 1.0f, SystemClock.elapsedRealtime())
                    .build();

            mMediaSession.setPlaybackState(state);
        } catch (Exception e) {

        }
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null && action.equalsIgnoreCase(BluetoothDevice.ACTION_ACL_DISCONNECTED)) {
                if (player != null && player.isPlaying()) {
                    stopPlaying();
                    isBluetoothDisconnected = true;
                    currentSeekPosition = 0;
                }
            }
        }
    };

}
